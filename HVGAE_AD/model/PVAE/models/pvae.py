import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as dist
from torch.utils.data import DataLoader

import math
from HVGAE_AD.model.PVAE.models.vae import VAE

from HVGAE_AD.model.PVAE.distributions import RiemannianNormal, WrappedNormal
from torch.distributions import Normal
import HVGAE_AD.model.PVAE.manifolds as manifolds
from HVGAE_AD.model.PVAE.models.architectures import EncWrapped, DecWrapped, EncMob, DecMob, DecGeo
from HVGAE_AD.model.PVAE.utils import get_activation

class PVAE(VAE):
    def __init__(self, params):
        c = nn.Parameter(params.curvature * torch.ones(1), requires_grad=False)
        manifold = getattr(manifolds, 'PoincareBall')(params.nout, c)
        super(PVAE, self).__init__(
            eval(params.prior),           # prior distribution
            eval(params.posterior),       # posterior distribution
            dist.Normal,                  # likelihood distribution
            eval('Enc' + params.enc)(params.curvature,params,manifold, params.data_size, get_activation(params), params.num_layers, params.nhid, params.prior_iso),
            eval('Dec' + params.dec)(manifold, params.data_size, get_activation(params), params.num_layers, params.nhid),
            params,
            manifold,
            c
        )
        self.manifold = manifold
        self._pz_mu = nn.Parameter(torch.zeros(1, params.nout), requires_grad=False)
        self._pz_logvar = nn.Parameter(torch.zeros(1, 1), requires_grad=params.learn_prior_std)

    @property
    def pz_params(self):
        return self._pz_mu.mul(1), F.softplus(self._pz_logvar).div(math.log(2)).mul(self.prior_std), self.manifold