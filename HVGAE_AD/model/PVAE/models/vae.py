# Base VAE class definition

import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as dist
from HVGAE_AD.model.PVAE.utils import get_mean_param
from torch.nn import Parameter
from torch_geometric.nn.inits import glorot


class VAE(nn.Module):
    def __init__(self, prior_dist, posterior_dist, likelihood_dist, enc, dec, params,manifold,c):
        super(VAE, self).__init__()
        self.c = c
        self.manifold = manifold
        self.pz = prior_dist
        self.px_z = likelihood_dist
        self.qz_x = posterior_dist
        self.enc = enc
        self.dec = dec
        self.modelName = None
        self.params = params
        self.data_size = params.data_size
        self.prior_std = params.prior_std

        if self.px_z == dist.RelaxedBernoulli:
            self.px_z.log_prob = lambda self, value: \
                -F.binary_cross_entropy_with_logits(
                    self.probs if value.dim() <= self.probs.dim() else self.probs.expand_as(value),
                    value.expand(self.batch_shape) if value.dim() <= self.probs.dim() else value,
                    reduction='none'
                )
            
        self.device = params.device
        self.hidden_initial = torch.ones(params.num_nodes, params.nout).to(params.device)
        self.gru = nn.GRUCell(params.nout, params.nout)
        self.nhid = params.nhid
        self.nout = params.nout
        self.cat = True
        self.Q = Parameter(torch.ones((params.nout, params.nhid)), requires_grad=True)
        self.r = Parameter(torch.ones((params.nhid, 1)), requires_grad=True)
        self.num_window = params.nb_window
        self.reset_parameters()
        self.cls = nn.Linear(params.nout, params.nclasses, True,device='cuda')

    def reset_parameters(self):
        glorot(self.Q)
        glorot(self.r)
        glorot(self.hidden_initial)

    def init_hiddens(self):
        self.hiddens = [self.initHyperX(self.hidden_initial)] * self.num_window
        return self.hiddens
    
    def initHyperX(self, x, c=1.0):
        return self.toHyperX(x, c)
    
    #### find the correspondant functions
    def toHyperX(self, x, c=1.0):
        # x_tan = self.manifold.proj_tan0(x, c)
        # x_hyp = self.manifold.expmap0(x_tan, c)
        # x_hyp = self.manifold.proj(x_hyp, c)
        x_hyp = self.manifold.expmap0(x)
        return x_hyp

    def weighted_hiddens(self, hidden_window):
        # temporal self-attention
        e = torch.matmul(torch.tanh(torch.matmul(hidden_window, self.Q)), self.r)
        e_reshaped = torch.reshape(e, (self.num_window, -1))
        a = F.softmax(e_reshaped, dim=0).unsqueeze(2)
        hidden_window_new = torch.reshape(hidden_window, [self.num_window, -1, self.nout])
        s = torch.mean(a * hidden_window_new, dim=0) # torch.sum is also applicable
        return s
    
    def toTangentX(self, x, c=1.0):
        #x = self.manifold.proj_tan0(self.manifold.logmap0(x, c), c)
        x = self.manifold.logmap0(x)
        return x

    def update_hiddens_all_with(self, z_t):
        self.hiddens.pop(0)  # [element0, element1, element2] remove the first element0
        self.hiddens.append(z_t.clone().detach().requires_grad_(False))  # [element1, element2, z_t]
        return z_t

    def generate(self, N, K):
        self.eval()
        with torch.no_grad():
            mean_pz = get_mean_param(self.pz_params)
            mean = get_mean_param(self.dec(mean_pz))
            px_z_params = self.dec(self.pz(*self.pz_params).sample(torch.Size([N])))
            means = get_mean_param(px_z_params)
            samples = self.px_z(*px_z_params).sample(torch.Size([K]))

        return mean, \
            means.view(-1, *means.size()[2:]), \
            samples.view(-1, *samples.size()[3:])

    def reconstruct(self, data , edge_index):
        self.eval()
        with torch.no_grad():
            qz_x = self.qz_x(*self.enc(edge_index,data))
            px_z_params = self.dec(qz_x.rsample(torch.Size([1])).squeeze(0))

        return get_mean_param(px_z_params)

    def forward(self, x , edge_index, K=1):
        embeddings = self.enc(edge_index,x)
        qz_x = self.qz_x(*embeddings)
        zs = qz_x.rsample(torch.Size([K]))
        px_z = self.px_z(*self.dec(zs))

        # GRU layer
        hlist = torch.cat([self.manifold.logmap0(hidden) for hidden in self.hiddens], dim=0)
        h = self.weighted_hiddens(hlist)
        embeddings = self.gru(embeddings[0], h)
        embeddings = self.toHyperX(embeddings, self.c)

        return qz_x, px_z, zs , embeddings

    @property
    def pz_params(self):
        return self._pz_mu.mul(1), F.softplus(self._pz_logvar).div(math.log(2)).mul(self.prior_std_scale)

    def decode(self,x):
        x = self.toTangentX(x, self.c)
        return F.log_softmax(self.cls(x), dim=1)