import torch
import torch.distributions as dist
from numpy import prod
from HVGAE_AD.model.PVAE.utils import has_analytic_kl, log_mean_exp
import torch.nn.functional as F

def vae_objective(model, x,qz_x, px_z, zs, beta=1.0,analytical_kl=False, **kwargs):
    """Computes E_{p(x)}[ELBO] """
    flat_rest = torch.Size([*px_z.batch_shape[:2], -1])
    x = x.unsqueeze(0).unsqueeze(2)
    lpx_z = px_z.log_prob(x.expand(px_z.batch_shape)).view(flat_rest).sum(-1)
    pz = model.pz(*model.pz_params)
    kld = dist.kl_divergence(qz_x, pz).unsqueeze(0).sum(-1) if \
        has_analytic_kl(type(qz_x), model.pz) and analytical_kl else \
        qz_x.log_prob(zs).sum(-1) - pz.log_prob(zs).sum(-1)
    lpx_z_selected = lpx_z
    kld_selected = kld
    obj = -lpx_z_selected.mean(0).sum() + beta * kld_selected.mean(0).sum()
    return obj