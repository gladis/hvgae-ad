import random
import torch
import numpy as np
import logging
from HVGAE_AD.config import args

def init_logger(log_file=None):
    log_format = logging.Formatter("[%(asctime)s %(levelname)s] %(message)s")
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_format)
    logger.handlers = [console_handler]
    # if log_file and log_file != '':
    #     file_handler = logging.FileHandler(log_file)
    #     file_handler.setFormatter(log_format)
    #     logger.addHandler(file_handler)
    return logger


logger = init_logger(args.log_file)


def set_random(random_seed):
    random.seed(random_seed)
    np.random.seed(random_seed)
    torch.manual_seed(random_seed)
    torch.cuda.manual_seed(random_seed)
    torch.cuda.manual_seed_all(random_seed)
    torch.backends.cudnn.deterministic = True
    # torch.use_deterministic_algorithms(True)
    logger.info('fixed random seed')