import numpy as np
import torch

import pickle
from scipy.sparse import csr_matrix
import scipy.sparse as sp


def sparse_to_tuple(sparse_mx):
    if not sp.isspmatrix_coo(sparse_mx):
        sparse_mx = sparse_mx.tocoo()
    coords = np.vstack((sparse_mx.row, sparse_mx.col)).transpose()
    values = sparse_mx.data
    shape = sparse_mx.shape
    return coords, values, shape

def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo()
    indices = torch.from_numpy(
            np.vstack((sparse_mx.row, sparse_mx.col)).astype(np.int64)
    )
    values = torch.Tensor(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)

def prepare(t,device,data_path):

    path = '{}labels_{}.pkl'.format(data_path,t)
    with open(path,'rb') as f:
        labels = pickle.load(f)

    path = '{}features_{}.pkl'.format(data_path,t)
    with open(path,'rb') as f:
        features = pickle.load(f)

    path = '{}adjacency_{}.pkl'.format(data_path,t)
    with open(path,'rb') as f:
        adj = pickle.load(f)
    adj = sparse_mx_to_torch_sparse_tensor(sp.csr_matrix(adj)).to(device)
    features = torch.from_numpy(features).float().to(device)
    labels = torch.from_numpy(labels).long().to(device)
    return adj,features,labels