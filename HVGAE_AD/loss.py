import torch.nn as nn
import torch.nn.functional as F
import HVGAE_AD.model.PVAE.objectives as objectives


class Loss(nn.Module):
    def __init__(self, args, c,model):
        super(Loss, self).__init__()
        self.args = args
        self.device = self.args.device
        self.c = c
        self.model = model
        self.reconstruction_loss = getattr(objectives,'vae_objective')

    def forward(self,embeddings,labels,features,qz_x, px_z, zs):
        loss_rec = self.reconstruction_loss(model=self.model,x=features,qz_x=qz_x, px_z=px_z, zs=zs, beta=self.args.beta, analytical_kl=self.args.analytical_kl)
        print('Reconstruction loss:',loss_rec.item())
        output = self.model.decode(embeddings)
        loss_cla = F.nll_loss(output, labels)
        print('Classification loss:',loss_cla.item())
        return self.args.gamma * loss_rec +  loss_cla