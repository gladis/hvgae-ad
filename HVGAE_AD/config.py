import argparse
import torch
import os


parser = argparse.ArgumentParser(description='HVAENCDG, hyperbolic variational autoencoder for node classification of dynamic graphs')

parser.add_argument('--gamma', type=float, default=0.00001, help='reconstruction importance')# ddos = 0.001 , darknet = 0.00001 (0 don't give better result) , ton iot = 0.0001
parser.add_argument('--nfeat', type=int, default=75, help='dim of input feature')
parser.add_argument('--nb_window', type=int, default=5)
parser.add_argument('--nhid', type=int, default=75, help='dim of hidden embedding')
parser.add_argument('--nout', type=int, default=2, help='dim of output embedding')
parser.add_argument('--act', type=str, default='relu')
parser.add_argument('--timelength', type=int, default=26, help='total number of snapshots')
parser.add_argument('--testlength', type=int, default=.3, help='number of test snapshots')
parser.add_argument('--num_nodes', type=int, default=1000, help='number of nodes per graph')
parser.add_argument('--nclasses', type=int, default=2, help='number of classes')
parser.add_argument('--lr', type=float, default=0.001, help='learning rate') # ddos,darknet,ton_iot = 0.001
parser.add_argument('--max_epoch', type=int, default=20, help='number of epochs to train.')
parser.add_argument('--patience', type=int, default=20, help='patience for early stop')
parser.add_argument('--min_epoch', type=int, default=5, help='min epoch')
parser.add_argument('--weight_decay', type=float, default=0.01, help='weight for L2 loss on basic model.')
parser.add_argument('--dropout', type=float, default=0.1, help='dropout rate (1 - keep probability).')
parser.add_argument('--curvature', type=float, default=1.0, help='curvature value')
parser.add_argument('--num_layers', type=int, default=2)
parser.add_argument('--beta1', type=float, default=0.9)
parser.add_argument('--beta2', type=float, default=0.999)
parser.add_argument('--K', type=int, default=1)
parser.add_argument('--beta', type=float, default=.2)
parser.add_argument('--analytical_kl', type=bool, default=True)
parser.add_argument('--posterior', type=str, default='WrappedNormal')
parser.add_argument('--prior', type=str, default='WrappedNormal')
parser.add_argument('--prior_iso', type=bool, default=True)
parser.add_argument('--prior_std', type=float, default=1.0)
parser.add_argument('--learn_prior_std', type=bool, default=True)
parser.add_argument('--enc', type=str, default='Mob')
parser.add_argument('--dec', type=str, default='Geo')
parser.add_argument('--bias', type=bool, default=True)
parser.add_argument('--alpha', type=float, default=.5)
parser.add_argument('--data_pt_path', type=str, default='./data/', help='parent path of dataset')
parser.add_argument('--device', type=int, default=0, help='gpu id, -1 for cpu')
parser.add_argument('--seed', type=int, default=42, help='random seed')
parser.add_argument('--output_pt_path', type=str, default='./output/', help='parent path of output')
parser.add_argument('--manifold', type=str, default='PoincareBall', help='hyperbolic model')
parser.add_argument('--eps', type=float, default=1e-15, help='eps')
parser.add_argument('--eval_while_testing',type=bool,default=True)


args, unknown = parser.parse_known_args()

if args.device >= 0 and torch.cuda.is_available():
    args.device = torch.device('cuda:{}'.format(args.device))
else:
    args.device = torch.device('cpu')

args.output_path = os.path.join(args.output_pt_path, 'ddos2019')
if not os.path.isdir(args.output_path):
    os.makedirs(args.output_path)
args.log_file = os.path.join(args.output_path, '{}.log'.format('HVGAE_AD'))
args.emb_file = os.path.join(args.output_path, '{}.emb'.format('HVGAE_AD'))
