import torch
import numpy as np
import time
from math import isnan
from torch import optim
import torch.optim.lr_scheduler as lr_scheduler
from HVGAE_AD.config import args
from HVGAE_AD.utils.data_utils import prepare
from HVGAE_AD.utils.util import set_random, logger
from HVGAE_AD.model.PVAE.models.pvae import PVAE
from HVGAE_AD.loss import Loss
from sklearn.metrics import accuracy_score, f1_score, recall_score,precision_score,roc_auc_score
import warnings
warnings.filterwarnings('ignore')

class HVGAE_AD(object):
    def __init__(self,data_path):
        self.data_path = data_path
        args.data_size = [args.num_nodes,args.nfeat]
        self.train_shots = list(range(0, args.timelength - int(args.testlength * args.timelength)))
        self.test_shots = list(range(int(args.testlength * args.timelength), args.timelength))
        self.model = PVAE(args).to(args.device)
        self.optimizer = optim.Adam(self.model.parameters(), lr=args.lr, amsgrad=True, betas=(args.beta1, args.beta2))
        self.scheduler = lr_scheduler.StepLR(self.optimizer, step_size=20, gamma=0.1)
        self.loss = Loss(args, self.model.c,self.model)
        set_random(args.seed)

    def calculate_metrics(self,preds,labels):
        labels = labels.cpu().detach().numpy()
        preds = preds.cpu().detach().numpy()
        preds = np.argmax(preds,axis=1)
        f1 = f1_score(labels, preds)
        accuracy = accuracy_score(labels, preds)
        recall = recall_score(labels, preds)
        precision = precision_score(labels, preds)
        roc_auc = roc_auc_score(labels,preds )
        return f1,accuracy,precision,recall,roc_auc
    
    def fit(self):
        print('Using device {} to train the model ...'.format(args.device))
        t_total0 = time.time()
        test_results, min_loss = [0] * 5, 10**7
        self.model.train()
        patience = 0
        for epoch in range(1, args.max_epoch + 1):
            t0 = time.time()
            epoch_losses = []
            self.model.init_hiddens()
            self.model.train()
            for t in self.train_shots:
                edge_index,features,labels= prepare(t,args.device,self.data_path)
                self.optimizer.zero_grad()
                qz_x, px_z, zs,z = self.model(features, edge_index,args.K)
                epoch_loss = self.loss(z,labels,features,qz_x, px_z, zs)
                logger.info('Epoch:{},  Snapshot: {}; Loss: {:.4f}'.format(epoch, t, epoch_loss.item()))
                epoch_loss.backward()
                self.optimizer.step()
                epoch_losses.append(epoch_loss.item())
                self.model.update_hiddens_all_with(z)
            self.scheduler.step()
            lr = self.optimizer.param_groups[0]["lr"]
            average_epoch_loss = np.mean(epoch_losses)
            gpu_mem_alloc = torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0
            
            logger.info('==' * 45)
            logger.info("Epoch:{}, LR: {:.4f}, Loss: {:.4f}, Time: {:.3f}, GPU: {:.1f}MiB".format(epoch, lr,average_epoch_loss,time.time() - t0,gpu_mem_alloc))
            
            if average_epoch_loss < min_loss:
                min_loss = average_epoch_loss
                if args.eval_while_testing:
                    test_results = self.predict()
                    logger.info('Epoch:{}, Accuracy: {:.4f}; F1: {:.4f}; Precision: {:.4f}; Recall: {:.4f}; ROC AUC: {:.4f}'.format(epoch, test_results[1], test_results[0],test_results[2],test_results[3],test_results[4]))
                patience = 0
            else:
                patience += 1
                if epoch > args.min_epoch and patience > args.patience:
                    print('early stopping')
                    break


            if isnan(epoch_loss):
                print('nan loss')
                break

        logger.info('>> Total time : %6.2f' % (time.time() - t_total0))
        logger.info(">> Parameters: lr:%.4f |Dim:%d |Window:%d |" % (args.lr, args.nout, args.nb_window))

    def predict(self):
        f1_list,acc_list,pre_list,rec_list,roc_list,memory,ptime = [], [], [], [], [],[],[]
        self.model.eval()
        for t in self.test_shots:
            edge_index, features,labels = prepare(t,args.device,self.data_path)
            start = time.time()
            qz_x, px_z, zs, embeddings = self.model(features,edge_index,args.K)
            preds = self.model.decode(embeddings)
            ptime.append(time.time() - start)
            memory.append(torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0)
            f1,accuracy,precision,recall,roc_auc = self.calculate_metrics(preds, labels)
            f1_list.append(f1)
            acc_list.append(accuracy)
            pre_list.append(precision)
            rec_list.append(recall)
            roc_list.append(roc_auc)

        return np.mean(f1_list), np.mean(acc_list), np.mean(pre_list), np.mean(rec_list), np.mean(roc_list),np.mean(memory),np.mean(ptime)  