import torch
import numpy as np
import time
import geoopt
import networkx as nx
from math import isnan

from config import args
from utils.data_utils import prepare
from utils.util import set_random, logger
from model import HGWaveNet
from loss import ReconLoss

from sklearn.metrics import accuracy_score, f1_score, recall_score,precision_score,roc_auc_score


class Trainer(object):
    def __init__(self):
        args.num_nodes = args.num_nodes
        self.train_shots = list(range(0, args.timelength - int(args.testlength * args.timelength)))
        self.test_shots = list(range(int(args.testlength * args.timelength), args.timelength))
        self.model = HGWaveNet(args).to(args.device)
        self.loss = ReconLoss(args, self.model.c_out,self.model)
        self.optimizer = geoopt.optim.radam.RiemannianAdam(self.model.parameters(), lr=args.lr,
                                                               weight_decay=args.weight_decay)
        set_random(args.seed)

    def calculate_metrics(self,z,labels):
        preds = self.model.decode(z)
        labels = labels.cpu().detach().numpy()
        preds = preds.cpu().detach().numpy()
        preds = np.argmax(preds,axis=1)
        f1 = f1_score(labels, preds)
        accuracy = accuracy_score(labels, preds)
        recall = recall_score(labels, preds)
        precision = precision_score(labels, preds)
        roc_auc = roc_auc_score(labels,preds )
        return f1,accuracy,precision,recall,roc_auc

    def train(self):
        t_total = time.time()
        min_loss = 1.0e8
        patience = 0
        for epoch in range(1, args.max_epoch + 1):
            t_epoch = time.time()
            epoch_losses = []
            z = None
            epoch_loss = None
            self.model.init_history()
            self.model.train()
            for t in self.train_shots:
                dilated_edge_index,features,labels = prepare(t,args.spatial_dilated_factors,args.device)
                self.optimizer.zero_grad()
                z = self.model(dilated_edge_index,features)
                epoch_loss = self.loss(z, labels) + self.model.htc(z)
                logger.info('Epoch:{},  Snapshot: {}; Loss: {:.4f}'.format(epoch, t, epoch_loss.item()))
                epoch_loss.backward()
                if isnan(epoch_loss):
                    logger.info('==' * 45)
                    logger.info('nan loss')
                    break
                self.optimizer.step()
                epoch_losses.append(epoch_loss.item())
                self.model.update_history(z)
            if isnan(epoch_loss):
                break
            gpu_mem_alloc = torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0
            average_epoch_loss = np.mean(epoch_losses)
            if average_epoch_loss < min_loss:
                min_loss = average_epoch_loss
                patience = 0
            else:
                patience += 1
                if epoch > args.min_epoch and patience > args.patience:
                    logger.info('==' * 45)
                    logger.info('early stopping!')
                    break
            if epoch == 1 or epoch % args.log_interval == 0:
                test_results = self.test()
                logger.info('==' * 45)
                logger.info("Epoch:{}, Loss: {:.4f}, Time: {:.3f}, GPU: {:.1f}MiB".format(epoch, average_epoch_loss,
                                                                                            time.time() - t_epoch,
                                                                                            gpu_mem_alloc))
                logger.info('Epoch:{}, Accuracy: {:.4f}; F1: {:.4f}; Recall: {:.4f}; Precision: {:.4f}; ROC AUC: {:.4f}, Memory: {:.4f}, time: {:.4f}'.format(epoch, test_results[1], test_results[0],test_results[2],test_results[3],test_results[4],test_results[5],test_results[6]))
        
        logger.info('==' * 45)
        logger.info('Total time: {:.3f}'.format(time.time() - t_total))

    def test(self):
        f1_list,acc_list,pre_list,rec_list,roc_list,occupied_memory,ptime = [], [], [], [], [],[],[]
        self.model.eval()
        for t in self.test_shots:
            edge_index,features,labels = prepare(t,args.spatial_dilated_factors,args.device)
            start = time.time()
            embeddings = self.model(edge_index,features)
            f1,accuracy,precision,recall,roc_auc = self.calculate_metrics(embeddings, labels)
            ptime.append(time.time() - start)
            occupied_memory.append(torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0)
            f1_list.append(f1)
            acc_list.append(accuracy)
            pre_list.append(precision)
            rec_list.append(recall)
            roc_list.append(roc_auc)
        return np.mean(f1_list), np.mean(acc_list), np.mean(rec_list), np.mean(pre_list),  np.mean(roc_list), np.mean(occupied_memory),np.mean(ptime) * 1000


if __name__ == '__main__':
    trainer = Trainer()
    trainer.train()
    print(trainer.test())
    