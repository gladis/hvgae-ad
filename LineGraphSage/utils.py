import torch
from torch_geometric.data import Data
import numpy as np


def filling_adjacency_numpy(data):
        N = data.shape[0]
        try:
            adjacency = np.zeros((N,N), dtype=bool)
        except Exception as e:
            print(f"An error occurred: {e}")

        source_ips = data[' Source IP'].to_numpy()
        destination_ips = data[' Destination IP'].to_numpy()
        mask = ((source_ips[:, np.newaxis] == source_ips) | (source_ips[:, np.newaxis] == destination_ips) | (destination_ips[:, np.newaxis] == source_ips)| (destination_ips[:, np.newaxis] == destination_ips) )
        adjacency[mask] = True
        return adjacency


def split_data(labels, val_prop, test_prop, seed):
    np.random.seed(seed)
    pos_idx = labels.nonzero()[0]
    neg_idx = (1. - labels).nonzero()[0]
    np.random.shuffle(pos_idx)
    np.random.shuffle(neg_idx)
    pos_idx = pos_idx.tolist()
    neg_idx = neg_idx.tolist()
    nb_pos_neg = min(len(pos_idx), len(neg_idx))
    nb_val = round(val_prop * nb_pos_neg)
    nb_test = round(test_prop * nb_pos_neg)
    idx_val_pos, idx_test_pos, idx_train_pos = pos_idx[:nb_val], pos_idx[nb_val:nb_val + nb_test], pos_idx[
                                                                                                   nb_val + nb_test:]
    idx_val_neg, idx_test_neg, idx_train_neg = neg_idx[:nb_val], neg_idx[nb_val:nb_val + nb_test], neg_idx[
                                                                                                   nb_val + nb_test:]
    return idx_val_pos + idx_val_neg, idx_test_pos + idx_test_neg, idx_train_pos + idx_train_neg


def create_graph(adj,features,labels,seed):
    non_zero_indices = np.nonzero(adj)
    indices = torch.tensor(non_zero_indices, dtype=torch.long)
    values = torch.tensor(adj[non_zero_indices], dtype=torch.float32)
    size = torch.Size(adj.shape)
    edge_index = torch.sparse_coo_tensor(indices, values, size).to_sparse_csr()
    idx_train,idx_val,idx_test = split_data(labels,val_prop=.2,test_prop=.3,seed=seed)
    labels = torch.tensor(labels).type(torch.LongTensor)
    features = torch.tensor(features,dtype=torch.float32)
    G_torch = Data(x=features, edge_index=edge_index, y=labels).cuda()
    return G_torch,idx_train,idx_val,idx_test

from sklearn.metrics import accuracy_score,f1_score,recall_score,precision_score,roc_auc_score

def compute_metrics(pred, labels):
    pred = pred.argmax(1).cpu().detach().numpy()
    labels = labels.cpu().detach().numpy()
    return accuracy_score(labels,pred),f1_score(labels,pred),recall_score(labels,pred),precision_score(labels,pred),roc_auc_score(labels,pred)