import pickle
import os
from linegraph import LineGraph

def load_samples():
        adj_path = os.path.join(os.getcwd(),'datasets','unsw_nb','adjacency.pkl')
        features_path = os.path.join(os.getcwd(),'datasets','unsw_nb','features.pkl')
        labels_path = os.path.join(os.getcwd(),'datasets','unsw_nb','labels.pkl')
        with open(adj_path,'rb') as f:
            adj = pickle.load(f)
        with open(features_path,'rb') as f:
            features = pickle.load(f)
        with open(labels_path,'rb') as f:
            labels = pickle.load(f)
        print('features:',features.shape,'adj',adj.shape,'labels',labels.shape)
        return adj,features,labels

if __name__ == "__main__":
    adj,features,labels = load_samples()
    model = LineGraph(adj=adj,features=features,labels=labels,dim=20)
    loss,acc,f1,recall,precision,roc_auc,training_time = model.fit()
    print("------------------------------")
    print('train:',acc,f1,recall,precision,roc_auc,training_time)
    loss,acc,f1,recall,precision,roc_auc = model.predict()
    print('test:',acc,f1,recall,precision,roc_auc)
    