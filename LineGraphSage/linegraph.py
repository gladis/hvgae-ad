import torch.nn as nn
import torch as th
from torch_geometric.nn import GraphSAGE
from utils import create_graph,compute_metrics
import warnings
import pandas as pd
import time
import numpy as np
warnings.simplefilter('ignore')

class LineGraph:

    def __init__(self,adj,features,labels,dim,epochs=50,seed=42):
        self.G_torch,self.idx_train,self.idx_val,self.idx_test = create_graph(adj,features,labels,seed)
        self.epochs = epochs
        self.model = GraphSAGE(
            in_channels = features.shape[1],
            hidden_channels = dim,
            out_channels = 2,
            num_layers = 2,
            dropout=0.1    
        ).cuda()

        self.criterion = nn.CrossEntropyLoss()
        self.opt = th.optim.Adam(self.model.parameters())

    def fit(self):
        start = time.time()
        train_losses,best_losses,val_losses = [],[],[]
        for epoch in range(self.epochs):
            self.model.train()
            self.opt.zero_grad()
            pred = self.model(self.G_torch.x, self.G_torch.edge_index)
            loss = self.criterion(pred[self.idx_train], self.G_torch.y[self.idx_train])
            train_losses.append(loss.item())
            if(len(best_losses) == 0):
                best_losses.append(train_losses[0])
            elif (best_losses[-1] > train_losses[-1]):
                best_losses.append(train_losses[-1])
            else:
                best_losses.append(best_losses[-1])
            loss.backward()
            self.opt.step()

            print(f'Epoch: {epoch}',compute_metrics(pred[self.idx_train], self.G_torch.y[self.idx_train]))

            self.model.eval()
            pred = self.model(self.G_torch.x, self.G_torch.edge_index)
            loss = self.criterion(pred[self.idx_val], self.G_torch.y[self.idx_val])
            val_losses.append(loss.item())
            acc,f1,recall,precision,roc_auc = compute_metrics(pred[self.idx_val], self.G_torch.y[self.idx_val])
            print('val:',acc,f1,recall,precision,roc_auc)

        return {'train':train_losses,'best':best_losses,'val':val_losses},acc,f1,recall,precision,roc_auc,time.time() - start

    def predict(self):
        self.model.eval()
        pred = self.model(self.G_torch.x, self.G_torch.edge_index)
        loss = self.criterion(pred[self.idx_test], self.G_torch.y[self.idx_test])
        acc,f1,recall,precision,roc_auc = compute_metrics(pred[self.idx_test], self.G_torch.y[self.idx_test])
        return loss,acc,f1,recall,precision,roc_auc
