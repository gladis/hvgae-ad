import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import torch
import os
import math
from linegraph import LineGraph
import pickle 

def load_samples(r,dataset):
        adj_path = os.path.join(os.getcwd(),'datasets',dataset,f'adjacency_{r}.pkl')
        features_path = os.path.join(os.getcwd(),'datasets',dataset,f'features_{r}.pkl')
        labels_path = os.path.join(os.getcwd(),'datasets',dataset,f'labels_{r}.pkl')
        with open(adj_path,'rb') as f:
            adj = pickle.load(f)
        with open(features_path,'rb') as f:
            features = pickle.load(f)
        with open(labels_path,'rb') as f:
            labels = pickle.load(f)
        print('features:',features.shape,'adj',adj.shape,'labels',labels.shape)
        return adj,features,labels

def testing(NAME,TEST_PROP,EPOCHS,EMBEDDING_DIMS,REPETITIONS,dataset):
    statistics = {}
    for dim in EMBEDDING_DIMS:
        print(f'START DIM {dim} ***************************')
        dim_statistics = []
        for r in range(REPETITIONS):
            torch.cuda.empty_cache()
            adj,features,labels = load_samples(r,dataset)
            model = None
            if(NAME == 'LineGraph'):
                model = LineGraph(adj=adj,features=features,labels=labels,dim=dim,epochs=EPOCHS)
            print(f"START REPETITION: {NAME} -------------------------------")
            loss_training,acc_training,f1_training,recall_training,precision_training,roc_auc_training,training_time = model.fit()
            loss_testing,acc_testing,f1_testing,recall_testing,precision_testing,roc_auc_testing = model.predict()
            stats = {'train':{'loss':loss_training,'acc':acc_training,'f1':f1_training,'recall':recall_training,'precision':precision_training,'roc_auc':roc_auc_training,'time':training_time},
                                        'test':{'loss':loss_testing,'acc':acc_testing,'f1':f1_testing,'recall':recall_testing,'precision':precision_testing,'roc_auc':roc_auc_testing}}
            dim_statistics.append(stats)
        del model
        statistics[f'{dim}'] = dim_statistics
    print('THE TESTING IS OVER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    return statistics

def calculate_avg_std(stats,list_scores,etiq):
    avgs = []
    stds = []
    for score in list_scores:
        all_values = []
        for repetition_stats in stats:
            all_values.append(repetition_stats[etiq][score])
        avgs.append(np.mean(all_values))
        # stds.append(np.std(all_values))
    return avgs,stds

def print_stats_per_dim(list_dim,stats,name,dataset):
    list_scores_train = ['acc','f1','recall','precision','roc_auc','time']
    list_scores_test = ['acc','f1','recall','precision','roc_auc']
    df = pd.DataFrame({})
    for i in range(len(list_dim)):
        dim = list_dim[i]
        dstats = stats[str(dim)]
        print("THE STATISTICS OF THE DIMENSION {}:".format(dim))
        print('',"TESING:","---------",sep='\n')
        avgs,stds = calculate_avg_std(dstats,list_scores_test,'test')
        for i in range(len(list_scores_test)):
            # print(f"{list_scores_test[i]}: avg={avgs[i]}, std={stds[i]}")
            df[f'{dim}_{list_scores_test[i]}_avg'] = [round(avgs[i],4)]
            # df[f'{dim}_{list_scores_test[i]}_ci'] = [round(1.96*stds[i]/math.sqrt(repetition),4)]
            # df[f'{dim}_{list_scores_test[i]}_std'] = [round(stds[i],4)]
        print('',"********************************","",sep='\n')
        print('',"TRAINING:","---------",sep='\n')
        avgs,stds = calculate_avg_std(dstats,list_scores_train,'train')
        # for i in range(len(list_scores_train)):
        #     print(f"{list_scores_train[i]}: avg={avgs[i]}, std={stds[i]}")
        df[f'{dim}_{list_scores_train[-1]}_avg'] = [round(avgs[-1],4)]
        # df[f'{dim}_{list_scores_train[-1]}_ci'] = [round(1.96*stds[-1]/math.sqrt(repetition),4)]
        # df[f'{dim}_{list_scores_train[-1]}_std'] = [round(stds[-1],4)]

    path = os.path.join(os.getcwd(),'results',name,f'{name}_{dataset}_statistics.csv')
    df.to_csv(path, index=False)


def plot_dim_loss(dim,stats,epochs,name,etiq,dataset):
    x = range(epochs)
    for i in range(len(stats)):
        y_real = stats[i]['train']['loss'][etiq]
        plt.plot(x, y_real,label=f'rep {i}')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend()
    path = os.path.join(os.getcwd(),'results',name,f'{name}_{dim}_{dataset}_{etiq}.png')
    plt.savefig(path)
    plt.close()

def plot_dim_loss_train_val(dim,stats,epochs,name,etiq,dataset):
    x = range(epochs)
    y_real = stats[-1]['train']['loss'][etiq]
    y_val = stats[-1]['train']['loss']['val']
    plt.plot(x, y_real,label=f'train')
    plt.plot(x, y_val,label=f'val')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend()
    path = os.path.join(os.getcwd(),'results',name,f'{name}_{dim}_{dataset}_{etiq}_val_train.png')
    plt.savefig(path)
    plt.close()

def plot_loss(statistics,name,dims,epochs):
    for dim in dims:
        plot_dim_loss(dim,statistics[str(dim)],epochs,name,'train')
        plot_dim_loss(dim,statistics[str(dim)],epochs,name,'best')
        plot_dim_loss_train_val(dim,statistics[str(dim)],epochs,name,'train')
        plot_dim_loss_train_val(dim,statistics[str(dim)],epochs,name,'best')



def do_it():
    EPOCHS = 50
    REPETITIONS = 5
    TEST_PROP = .3
    EMBEDDING_DIMS= [2,5,10,20]
    dataset= 'unsw_nb'
    NAME = 'LineGraph'
    statistics = testing(NAME,TEST_PROP,EPOCHS,EMBEDDING_DIMS,REPETITIONS,dataset)
    print_stats_per_dim(EMBEDDING_DIMS,statistics,name=NAME,dataset=dataset)
    # plot_loss(statistics,NAME,EMBEDDING_DIMS,EPOCHS)


do_it()