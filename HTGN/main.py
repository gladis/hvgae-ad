import os
import sys
import time
import torch
import numpy as np
from math import isnan
import warnings
import pickle
from sklearn.metrics import accuracy_score, f1_score, recall_score,precision_score,roc_auc_score

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)




class Runner(object):
    def __init__(self):
        self.train_shots = list(range(0, args.timelength - int(args.testlength * args.timelength)))
        self.test_shots = list(range(int(args.testlength * args.timelength), args.timelength))
        #self.load_feature()
        self.model = load_model(args).to(args.device)
        self.loss = ReconLoss(args,self.model)
        logger.info('total length: {}, test length: {}'.format(args.timelength, args.testlength))

    def calculate_metrics(self,z,labels):
        preds = self.model.decode(z)
        labels = labels.cpu().detach().numpy()
        preds = preds.cpu().detach().numpy()
        preds = np.argmax(preds,axis=1)
        f1 = f1_score(labels, preds)
        accuracy = accuracy_score(labels, preds)
        recall = recall_score(labels, preds)
        precision = precision_score(labels, preds)
        roc_auc = roc_auc_score(labels,preds )
        return f1,accuracy,precision,recall,roc_auc
    
    def optimizer(self, using_riemannianAdam=True):
        if using_riemannianAdam:
            import geoopt
            optimizer = geoopt.optim.radam.RiemannianAdam(self.model.parameters(), lr=args.lr,
                                                          weight_decay=args.weight_decay)
        else:
            import torch.optim as optim
            optimizer = optim.Adam(self.model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
        return optimizer

    def run(self):
        optimizer = self.optimizer()
        t_total0 = time.time()
        test_results, min_loss = [0] * 5, 10
        self.model.train()
        patience = 0
        for epoch in range(1, args.max_epoch + 1):
            t0 = time.time()
            epoch_losses = []
            self.model.init_hiddens()
            self.model.train()
            for t in self.train_shots:
                edge_index,features,labels= prepare(t)
                optimizer.zero_grad()
                z = self.model(edge_index, features)
                if args.use_htc == 0:
                    epoch_loss = self.loss(z,labels)
                else:
                    epoch_loss = self.loss(z,labels) + self.model.htc(z)
                epoch_loss.backward()
                optimizer.step()
                epoch_losses.append(epoch_loss.item())
                self.model.update_hiddens_all_with(z)
            gpu_mem_alloc = torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0
            average_epoch_loss = np.mean(epoch_losses)
            if average_epoch_loss < min_loss:
                min_loss = average_epoch_loss
                test_results = self.test()
                patience = 0
            else:
                patience += 1
                if epoch > args.min_epoch and patience > args.patience:
                    print('early stopping')
                    break

            logger.info('==' * 45)
            logger.info("Epoch:{}, Loss: {:.4f}, Time: {:.3f}, GPU: {:.1f}MiB".format(epoch, average_epoch_loss,
                                                                                        time.time() - t0,
                                                                                        gpu_mem_alloc))
            logger.info('Epoch:{}, Accuracy: {:.4f}; F1: {:.4f}; Precision: {:.4f}; Recall: {:.4f}; ROC AUC: {:.4f}'.format(epoch, test_results[1], test_results[0],test_results[2],test_results[3],test_results[4]))

            if isnan(epoch_loss):
                print('nan loss')
                sys.exit()
        logger.info('>> Total time : %6.2f' % (time.time() - t_total0))
        logger.info(">> Parameters: lr:%.4f |Dim:%d |Window:%d |" % (args.lr, args.nhid, args.nb_window))

    def test(self):
        f1_list,acc_list,pre_list,rec_list,roc_list,occupied_memory,processing_time = [], [], [], [], [],[],[]
        self.model.eval()
        for t in self.test_shots:
            edge_index, features,labels = prepare(t)
            start = time.time()
            embeddings = self.model(edge_index, features)
            f1,accuracy,precision,recall,roc_auc = self.calculate_metrics(embeddings, labels)
            processing_time.append(time.time() - start)
            occupied_memory.append(torch.cuda.max_memory_allocated() / 1000000 if torch.cuda.is_available() else 0)
            f1_list.append(f1)
            acc_list.append(accuracy)
            pre_list.append(precision)
            rec_list.append(recall)
            roc_list.append(roc_auc)

        return np.mean(f1_list), np.mean(acc_list),  np.mean(rec_list), np.mean(pre_list), np.mean(roc_list) , np.mean(occupied_memory),np.mean(processing_time) *1000


if __name__ == '__main__':
    warnings.filterwarnings("ignore")
    from HTGN.config import args
    from HTGN.utils.util import set_random, logger, init_logger
    from HTGN.models.load_model import load_model
    from HTGN.loss import ReconLoss
    from HTGN.utils.data_util import loader, prepare_dir
    from HTGN.inits import prepare

    set_random(args.seed)
    init_logger(prepare_dir(args.output_folder) + args.dataset + '.txt')
    runner = Runner()
    runner.run()
    print(runner.test())
