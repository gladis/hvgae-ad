import os
import numpy as np
import torch
from torch_geometric.utils import train_test_split_edges
from torch_geometric.data import Data
import pickle
from HTGN.utils.make_edges_orign import mask_edges_det, mask_edges_prd, mask_edges_prd_new_by_marlin
from HTGN.utils.make_edges_new import get_edges, get_prediction_edges, get_new_prediction_edges
from scipy.sparse import csr_matrix

def mkdirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)
    return path


def prepare_dir(output_folder):
    mkdirs(output_folder)
    log_folder = mkdirs(output_folder)
    return log_folder


def generate_adj_list_ddos2019(n_snapshots=10,new_link_per_snapshot=10):
    path = '../data/ddos2019/adjacency.pkl'
    with open(path,'rb') as f:
        adj = pickle.load(f)
    adj_list=[]
    adj_list.append(csr_matrix(adj))
    linked = list(np.where(adj == True))
    for _ in range(n_snapshots -1):
        for _ in range(new_link_per_snapshot):
            rand = np.random.randint(0,len(linked[0]))
            adj[linked[0][rand],linked[1][rand]]=False
            linked[0] = np.delete(linked[0], rand)
            linked[1] = np.delete(linked[1], rand)
        adj_list.append(csr_matrix(adj))
    adj_list = adj_list[::-1]
    return adj_list

def generate_features_ddos2019(args,n_snapshots=10):
    features_list = []
    path = '../data/ddos2019/features.pkl'
    with open(path,'rb') as f:
        features = pickle.load(f)
    for _ in range(n_snapshots):
        features_list.append(torch.from_numpy(features).float().to(args.device))
    return features_list

def generate_labels_list_ddos2019(args,n_snapshots=10):
    labels_list = []
    path = '../data/ddos2019/labels.pkl'
    with open(path,'rb') as f:
        labels = pickle.load(f)
    for _ in range(n_snapshots):
        labels_list.append(torch.from_numpy(labels).long())
    return labels_list

def load_vgrnn_dataset(args,dataset):
    # assert dataset in ['enron10', 'dblp']  # using vgrnn dataset
    # print('>> loading on vgrnn dataset')
    # with open('../data/input/raw/{}/adj_time_list.pickle'.format(dataset), 'rb') as handle:
    #     adj_time_list = pickle.load(handle, encoding='iso-8859-1')
    # print('>> generating edges,negative edges wait for a while ...')
    features_list = generate_features_ddos2019(args)
    adj_time_list = generate_adj_list_ddos2019()
    #labels_list = generate_labels_list_ddos2019(args)
    data = {}
    edges, biedges = mask_edges_det(adj_time_list)  # list
    #pedges, nedges = mask_edges_prd(adj_time_list)  # list
    print('>> processing finished!')
    #assert len(edges) == len(biedges) == len(pedges) == len(nedges)
    assert len(edges) == len(biedges)
    #edge_index_list, pedges_list, nedges_list = [], [], []
    edge_index_list= []
    for t in range(len(biedges)):
        edge_index_list.append(torch.tensor(np.transpose(biedges[t]), dtype=torch.long))
        # pedges_list.append(torch.tensor(np.transpose(pedges[t]), dtype=torch.long))
        # nedges_list.append(torch.tensor(np.transpose(nedges[t]), dtype=torch.long))

    data['edge_index_list'] = edge_index_list
    # data['pedges'], data['nedges'] = pedges_list, nedges_list
    #data['num_nodes'] = int(np.max(np.vstack(edges))) + 1
    data['features_list'] = features_list
    #data['labels_list'] = labels_list
    #data['time_length'] = len(edge_index_list)
    # data['weights'] = None
    #print('>> data: ddos2019')
    #print('>> total length:{}'.format(len(edge_index_list)))
    #print('>> number nodes: {}'.format(data['num_nodes']))
    return data


# def load_new_dataset(dataset):
#     print('>> loading on new dataset')
#     data = {}
#     rawfile = '../data/input/processed/{}/{}.pt'.format(dataset, dataset)
#     edge_index_list = torch.load(rawfile)  # format: list:[[[1,2],[2,3],[3,4]]]
#     undirected_edges = get_edges(edge_index_list)
#     num_nodes = int(np.max(np.hstack(undirected_edges))) + 1
#     pedges, nedges = get_prediction_edges(undirected_edges)  # list

#     data['edge_index_list'] = undirected_edges
#     data['pedges'], data['nedges'] = pedges, nedges
#     data['num_nodes'] = num_nodes
#     data['time_length'] = len(edge_index_list)
#     data['weights'] = None
#     print('>> data: {}'.format(dataset))
#     print('>> total length: {}'.format(len(edge_index_list)))
#     print('>> number nodes: {}'.format(data['num_nodes']))
#     return data


# def load_vgrnn_dataset_det(dataset):
#     assert dataset in ['enron10', 'dblp']  # using vgrnn dataset
#     print('>> loading on vgrnn dataset')
#     with open('../data/input/raw/{}/adj_time_list.pickle'.format(dataset), 'rb') as handle:
#         adj_time_list = pickle.load(handle, encoding='iso-8859-1')
#     print('>> generating edges, negative edges and new edges, wait for a while ...')
#     data = {}
#     edges, biedges = mask_edges_det(adj_time_list)  # list
#     pedges, nedges = mask_edges_prd(adj_time_list)  # list
#     print('>> processing finished!')
#     assert len(edges) == len(biedges) == len(pedges) == len(nedges) 
#     edge_index_list, pedges_list, nedges_list = [], [], []
#     for t in range(len(biedges)):
#         edge_index_list.append(torch.tensor(np.transpose(biedges[t]), dtype=torch.long))
#         pedges_list.append(torch.tensor(np.transpose(pedges[t]), dtype=torch.long))
#         nedges_list.append(torch.tensor(np.transpose(nedges[t]), dtype=torch.long))

#     data['edge_index_list'] = edge_index_list
#     data['pedges'], data['nedges'] = pedges_list, nedges_list
#     data['num_nodes'] = int(np.max(np.vstack(edges))) + 1

#     data['time_length'] = len(edge_index_list)
#     data['weights'] = None
#     print('>> data: {}'.format(dataset))
#     print('>> total length:{}'.format(len(edge_index_list)))
#     print('>> number nodes: {}'.format(data['num_nodes']))
#     return data


# def load_new_dataset_det(dataset):
#     print('>> loading on new dataset')
#     data = {}
#     rawfile = '../data/input/processed/{}/{}.pt'.format(dataset, dataset)
#     edge_index_list = torch.load(rawfile)  # format: list:[[[1,2],[2,3],[3,4]]]
#     undirected_edges = get_edges(edge_index_list)
#     num_nodes = int(np.max(np.hstack(undirected_edges))) + 1

#     gdata_list = []
#     for edge_index in undirected_edges:
#         gdata = Data(x=None, edge_index=edge_index, num_nodes=num_nodes)
#         gdata_list.append(train_test_split_edges(gdata, 0.1, 0.4))

#     data['gdata'] = gdata_list
#     data['num_nodes'] = num_nodes
#     data['time_length'] = len(edge_index_list)
#     data['weights'] = None
#     print('>> data: {}'.format(dataset))
#     print('>> total length: {}'.format(len(edge_index_list)))
#     print('>> number nodes: {}'.format(data['num_nodes']))
#     return data


def loader(args,dataset='ddos2019'):
    # data_root = '../data/input/cached/{}/'.format(dataset)
    # filepath = mkdirs(data_root) + '{}.data'.format(dataset)
    # if dataset in ['enron10', 'dblp']:
    #     data = load_vgrnn_dataset(dataset)
    # if dataset in ['as733', 'fbw', 'HepPh30', 'disease']:
    #     data = load_new_dataset(dataset)
    # torch.save(data, filepath)
    # print('saved!')
    data = load_vgrnn_dataset(args,dataset)
    return data
