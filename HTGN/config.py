import argparse
import torch
import os

parser = argparse.ArgumentParser(description='HTGN')


parser.add_argument('--nb_window', type=int, default=5, help='the length of window')
parser.add_argument('--dropout', type=float, default=0.1, help='dropout rate (1 - keep probability).')
parser.add_argument('--weight_decay', type=float, default=0.01, help='weight for L2 loss on basic models.')
parser.add_argument('--lr', type=float, default=0.00001, help='learning rate') # ton_iot evolveGCN = 0.01, darknet htgn= 0.1, ton_iot HTGN = 0.002, ddos htgn = 0.01

parser.add_argument('--nfeat', type=int, default=75, help='dim of input feature')
parser.add_argument('--num_nodes', type=int, default=1000, help='number of nodes per graph')
parser.add_argument('--timelength', type=int, default=26, help='total number of snapshots')
parser.add_argument('--testlength', type=int, default=.3, help='number of test snapshots')
parser.add_argument('--nhid', type=int, default=2, help='dim of hidden embedding')
parser.add_argument('--nout', type=int, default=2, help='dim of output embedding')

parser.add_argument('--curvature', type=float, default=1.0, help='curvature value')
parser.add_argument('--heads', type=int, default=1, help='attention heads.')

parser.add_argument('--seed', type=int, default=42, help='random seed')
parser.add_argument('--nclasses', type=int, default=2, help='number of classes')
parser.add_argument('--fixed_curvature', type=int, default=1, help='fixed (1) curvature or not (0)')
parser.add_argument('--max_epoch', type=int, default=20, help='number of epochs to train.')
parser.add_argument('--patience', type=int, default=20, help='patience for early stop')
parser.add_argument('--repeat', type=int, default=1, help='running times')
parser.add_argument('--dataset', type=str, default='ddos2019', help='datasets')
parser.add_argument('--aggregation', type=str, default='att', help='aggregation method: [deg, att]')

# 1.dataset

# 2.experiments
parser.add_argument('--device', type=str, default='gpu', help='training device')
parser.add_argument('--device_id', type=str, default='0', help='device id for gpu')
parser.add_argument('--output_folder', type=str, default='', help='need to be modified')
parser.add_argument('--use_htc', type=int, default=1, help='use htc or not, default: 1')
parser.add_argument('--use_hta', type=int, default=1, help='use hta or not, default: 1')
parser.add_argument('--debug_content', type=str, default='', help='debug_mode content')
parser.add_argument('--sampling_times', type=int, default=1, help='negative sampling times')
parser.add_argument('--log_interval', type=int, default=1, help='log interval, default: 20,[20,40,...]')
parser.add_argument('--pre_defined_feature', default=None, help='pre-defined node feature')
parser.add_argument('--save_embeddings', type=int, default=0, help='save or not, default:0')
parser.add_argument('--debug_mode', type=int, default=0, help='debug_mode, 0: normal running; 1: debugging mode')
parser.add_argument('--min_epoch', type=int, default=100, help='min epoch')

# 3.models
parser.add_argument('--model', type=str, default='HTGN', help='model name') #HTGN , EGCN
parser.add_argument('--manifold', type=str, default='PoincareBall', help='Hyperbolic model')
parser.add_argument('--use_gru', type=bool, default=True, help='use gru or not')
parser.add_argument('--use_hyperdecoder', type=bool, default=True, help='use hyperbolic decoder or not')
parser.add_argument('--EPS', type=float, default=1e-15, help='eps')
parser.add_argument('--bias', type=bool, default=True, help='use bias or not')
parser.add_argument('--trainable_feat', type=int, default=0,
                    help='using trainable feat or one-hot feat, default: none-trainable feat')
parser.add_argument('--egcn_type', type=str, default='EGCNO', help='Type of EGCN: EGCNH or EGCNO')
args = parser.parse_args()

# set the running device
if int(args.device_id) >= 0 and torch.cuda.is_available():
    args.device = torch.device("cuda:{}".format(args.device_id))
    print('using gpu:{} to train the model'.format(args.device_id))
else:
    args.device = torch.device(args.device)
    print('using cpu to train the model')

args.output_folder = '../data/output/log/{}/{}/'.format(args.dataset, args.model)
args.result_txt = '../data/output/results/{}_{}_result.txt'.format(args.dataset, args.model)
