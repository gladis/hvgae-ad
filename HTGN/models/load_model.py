from HTGN.utils.util import logger
from HTGN.models.HTGN import HTGN
from HTGN.models.EvolveGCN.EGCN import EvolveGCN
from HTGN.models.DynModels import DGCN

def load_model(args):
    if args.model in ['GRUGCN', 'DynGCN']:
        model = DGCN(args)
    elif args.model == 'HTGN':
        model = HTGN(args)
    elif args.model == 'EGCN':
        model = EvolveGCN(args)
    else:
        raise Exception('pls define the model')
    logger.info('using model {} '.format(args.model))
    return model
