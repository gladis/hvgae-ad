import math
import numpy as np
import torch
from HTGN.config import args


def uniform(size, tensor):
    bound = 1.0 / math.sqrt(size)
    if tensor is not None:
        tensor.data.uniform_(-bound, bound)


def xavier_init(shape):
    """Glorot & Bengio (AISTATS 2010) init."""
    init_range = np.sqrt(6.0 / (shape[0] + shape[1]))
    initial = np.random.uniform(low=-init_range, high=init_range, size=shape)
    return torch.Tensor(initial)


def glorot(tensor):
    if tensor is not None:
        stdv = math.sqrt(6.0 / (tensor.size(-2) + tensor.size(-1)))
        tensor.data.uniform_(-stdv, stdv)


def zeros(tensor):
    if tensor is not None:
        tensor.data.fill_(0)


def ones(tensor):
    if tensor is not None:
        tensor.data.fill_(1)

import pickle
from scipy.sparse import csr_matrix
import scipy.sparse as sp
from HTGN.utils.make_edges_orign import sparse_to_tuple

def prepare(t):
    # for EvolveGCN add ../
    path = '../data/snapshots/labels_{}.pkl'.format(t)
    with open(path,'rb') as f:  
        labels = pickle.load(f)

    path = '../data/snapshots/features_{}.pkl'.format(t)
    with open(path,'rb') as f:
        features = pickle.load(f)

    path = '../data/snapshots/adjacency_{}.pkl'.format(t)
    with open(path,'rb') as f:
        adj = pickle.load(f)

    adj = csr_matrix(adj)
    adj = adj - sp.dia_matrix((adj.diagonal()[np.newaxis, :], [0]), shape=adj.shape)
    adj.eliminate_zeros()
    assert np.diag(adj.todense()).sum() == 0
    biedges = sparse_to_tuple(adj)[0]
    #np.random.shuffle(biedges)

    edge_index = torch.tensor(np.transpose(biedges), dtype=torch.long).to(args.device)
    features = torch.from_numpy(features).float().to(args.device)
    labels = torch.from_numpy(labels).long().to(args.device)
    return edge_index,features,labels

def prepare_all_data(snapshots):
    adjacences = []
    features = []
    labels = []
    for t in snapshots:
        adj,feats,ls = prepare(t)
        adjacences.append(adj)
        features.append(feats)
        labels.append(ls)
    return adjacences,features,labels