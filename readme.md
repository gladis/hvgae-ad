# HVAE-AD

## 1. Overview

In this repository, you find our proposed approach for anomaly detection in cybersecurity using temporal graphs, hyperbolic embedding, and graph neural networks which we called it HVAE-AD (Hyperbolic Variational Autoencoder for Anomaly Detection). You also find both Euclidean (for static and dynamic graphs) and hyperbolic baselines to campare with.

The overall architecture of our approach is illustrated in the figure below.
![HVAE-AD Architecture](./images/dynamic.png)

### 1.1 Models

| Name          | Ref   |
|---------------|-------|
| DOMINANT      |  [1]  |
| LineGraphSage |  [2]  |
| HGWaveNet     |  [3]  |
| HTGN          |  [4]  |
| VGRNN         |  [5]  |
| EvolveGCN     |  [6]  |

### 1.2 Datasets

To evaluate our dynamic approach we generated snapshots from 03 different datasets:

| Name            | Ref  |
|-----------------|------|
| CIC-DDoS2019    | [7]  |
| CIC-Darknet2020 | [8]  |
| ToN-IoT         | [9]  |

## 2. Installation

```bash
git clone https://gitlab.liris.cnrs.fr/gladis/hvgae-ad.git
```

## 3. Usage
The snapshots of a given dataset have to be inside `data/snapshots/`.

- **HVAE-AD**:
All the arguments can be tweaked inside the file: `HVGAE_AD/config.py`.
```python
from HVGAE_AD import HVGAE_AD
directory = 'data/snapshots/'
model = HVGAE_AD(data_path=directory)
model.fit()
f1_score, accuracy, precision, recall, roc_auc, occupied_memory, processing_time = model.predict()
```

- **DOMINANT**:
The graphs have to be located in the folders inside `DOMINANT/datasets/`. The names of the matrices have to be like `matrix_0.pkl` where 0 indicated the first repetition. This helps train the model multiple time with different graphs to test the model over multiple samples.
```bash
cd DOMINANT
python test.py
```

- **LineGraphSage**:
Same as DOMINANT, the graphs have to be in `LineGraphSage/datasets/` and the names has to be like `name_0.pkl`.
```bash
cd LineGraphSage
python test.py
```

- **HGWaveNet**:
```bash
cd HGWaveNet
python main.py
```

- **HTGN**:
Make sure in `HTGN/config.py` the argument `--model` is `HTGN`.
```bash
cd HTGN
python main.py
```

- **EvolveGCN**:
We used the implementation available in the source code of HTGN. Set the argument `--model` inside `HTGN/config.py` as `EGCN`.
```bash
cd HTGN/baselines
python run_evolvegcn_baselines.py
```

- **VGRNN**:
```bash
cd VGRNN
python VGRNN_detection.py
```

## 4. References

[1]:[DOMINANT ](https://github.com/jackd/dominant)
[2]:[LineGraphSage ](https://github.com/andreaventuri01/Structural_attacks_GNN_NIDS)
[3]:[HGWaveNet ](https://github.com/TaiLvYuanLiang/HGWaveNet)
[4]:[HTGN ](https://github.com/marlin-codes/HTGN)
[5]:[VGRNN ](https://github.com/VGraphRNN/VGRNN)
[6]:[EvolveGCN ](https://github.com/IBM/EvolveGCN)
[7]:[CIC-DDoS2019 ](https://www.unb.ca/cic/datasets/ddos-2019.html)
[8]:[CIC-Darknet2020 ](https://www.unb.ca/cic/datasets/darknet2020.html)
[9]:[ToN-IoT ](https://research.unsw.edu.au/projects/toniot-datasets)