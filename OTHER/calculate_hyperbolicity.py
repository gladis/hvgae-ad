# NOTE: this file calcules the hyperbolicity of all the datasets


import numpy as np
from Ghypeddings.datasets.utils import hyperbolicity
from Ghypeddings.datasets.datasets import CIC_DDoS2019,AWID3,NF_CIC_IDS2018_v2,Darknet,NF_TON_IoT_v2,NF_BOT_IoT_v2,NF_UNSW_NB15_v2

datasets = [['ddos2019',CIC_DDoS2019],
            ['awid3',AWID3],
            ['ids2018',NF_CIC_IDS2018_v2],
            ['darknet',Darknet],
            ['ton_iot',NF_TON_IoT_v2],
            ['bot_iot',NF_BOT_IoT_v2],
            ['unsw_nb',NF_UNSW_NB15_v2]]
for dataset in datasets:
    h_mean = []
    for i in range(5):
        adj,_,_ = dataset[1]().load_samples(repetition=0)
        h = hyperbolicity(adj,num_samples=10)
        h_mean.append(h)
    print(dataset[0],np.mean(h_mean))