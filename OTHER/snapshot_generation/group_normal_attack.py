# NOTE: same logic with group_attack.py file it randomly select raws from normal or attacks while keeping the order inside each class


import pandas as pd
import os
import numpy as np



# file = os.path.join(os.path.dirname(os.path.abspath(__file__)),'Ghypeddings','datasets','examples','CICDDoS2019','original','all.csv')
file_normal = os.path.join(os.path.dirname(os.path.abspath(__file__)),'Ghypeddings','datasets','examples','CICDDoS2019','original','normal.csv')
file_attack = os.path.join(os.path.dirname(os.path.abspath(__file__)),'Ghypeddings','datasets','examples','CICDDoS2019','original','attack.csv')
for_snapshotting = os.path.join(os.path.dirname(os.path.abspath(__file__)),'Ghypeddings','datasets','examples','CICDDoS2019','original','for_snapshotting.csv')
# df = pd.read_csv(file,low_memory=False)
# print('>> shape:',df.shape)
# df.dropna(axis=0, inplace=True)
# normal = df[df[' Label'] == 'BENIGN']
# print('>> normal shape:',normal.shape)
# normal.to_csv(file_normal,index=False)
# attack = df[df[' Label'] != 'BENIGN']
# print('>> attack shape:',attack.shape)

# smallest = min(attack.shape[0],normal.shape[0])

# if normal.shape[0] <= attack.shape[0]:
#     normal = normal.sort_values(by=' Timestamp')
#     normal.to_csv(file_normal)
#     attack = attack.sample(n=normal.shape[0]).reset_index(drop=True)
#     attack = attack.sort_values(by=' Timestamp')
#     attack.to_csv(file_attack)
# else:
#     attack = attack.sort_values(by=' Timestamp')
#     attack.to_csv(file_attack)
#     normal = normal.sample(n=attack.shape[0]).reset_index(drop=True)
#     normal = normal.sort_values(by=' Timestamp')
#     normal.to_csv(file_normal)

df = pd.DataFrame()
normal = pd.read_csv(file_normal,low_memory=False)
attack = pd.read_csv(file_attack,low_memory=False)
attack[' Label'] = 1
normal[' Label'] = 0
nnodes = normal.shape[0]
i,j=0,0
stop =False
while not stop:
    if i < nnodes:
        k = np.random.randint(1,20)
        if i+k > nnodes:
            k = nnodes - i
        print('Normal: [{},{}]'.format(i,i+k))
        df = pd.concat([df,normal.iloc[i:i+k,:]],ignore_index=True)
        i+=k
    if j < nnodes:
        k = np.random.randint(1,20)
        if j+k > nnodes:
            k = nnodes - j
        print('Attack: [{},{}]'.format(j,j+k))
        df = pd.concat([df,attack.iloc[j:j+k,:]],ignore_index=True)
        j+=k
    if i == j == nnodes:
        stop = True

df.to_csv(for_snapshotting,index=False)
