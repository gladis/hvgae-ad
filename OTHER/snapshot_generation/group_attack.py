# NOTE: here we have only the script for the TON_IOT dataset but the rest of the dataset follow the same logic
# It startes by extract the classes of attacks and the normal behaviour and then sort them by class and then randomly select some raws from a randomly selected category
# the rows are sorted by the timestamp 

import pandas as pd
import os
import random

def _to_binary_classification(x):
    if 'Non' in x:
        return 0
    else:
        return 1


backdoor = pd.read_csv(os.path.join('ton_iot','ton_iot_backdoor.csv'))
ddos = pd.read_csv(os.path.join('ton_iot','ton_iot_ddos.csv'))
dos = pd.read_csv(os.path.join('ton_iot','ton_iot_dos.csv'))
mitm = pd.read_csv(os.path.join('ton_iot','ton_iot_mitm.csv'))
password = pd.read_csv(os.path.join('ton_iot','ton_iot_password.csv'))
ransomware = pd.read_csv(os.path.join('ton_iot','ton_iot_ransomware.csv'))
scanning = pd.read_csv(os.path.join('ton_iot','ton_iot_scanning.csv'))
xss = pd.read_csv(os.path.join('ton_iot','ton_iot_xss.csv'))
injection = pd.read_csv(os.path.join('ton_iot','ton_iot_injection.csv'))
print('backdoor',backdoor.shape[0])
print('ddos',ddos.shape[0])
print('dos',dos.shape[0])
print('mitm',mitm.shape[0])
print('password',password.shape[0])
print('ransomware',ransomware.shape[0])
print('scanning',scanning.shape[0])
print('xss',xss.shape[0])
print('injection',injection.shape[0])
m = backdoor.shape[0] + ddos.shape[0] + dos.shape[0] + mitm.shape[0] + password.shape[0] + ransomware.shape[0] + scanning.shape[0] + xss.shape[0] + injection.shape[0]
files = os.listdir(directory)

normal = pd.read_csv('ton_iot/normal.csv')
attack = pd.read_csv('ton_iot/attacks.csv')

m = normal.shape[0] + attack.shape[0]
all = pd.DataFrame()
i=0
while i< m:
    print(i,"-",m)
    if normal.shape[0]>0:
        k = random.randint(10,20)
        if normal.shape[0] >k:
            all = pd.concat([all,normal.iloc[:k,:]],axis=0)
            normal.drop(range(k),axis=0,inplace=True)
            normal = normal.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,normal],axis=0)
            i+=normal.shape[0]
            normal = pd.DataFrame()

    if attack.shape[0]>0:
        k = random.randint(10,20)
        if attack.shape[0] >k:
            all = pd.concat([all,attack.iloc[:k,:]],axis=0)
            attack.drop(range(k),axis=0,inplace=True)
            attack = attack.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,attack],axis=0)
            i+=attack.shape[0]
            attack = pd.DataFrame()

all.to_csv('ton_iot/all.csv',index=False)

all = pd.DataFrame()   
i = 0
while i < m:
    print(i,"/",m)
    if backdoor.shape[0] >0:
        k = random.randint(1,10)
        if(backdoor.shape[0] >k):
            all = pd.concat([all,backdoor.iloc[:k,:]],axis=0)
            backdoor.drop(range(k),axis=0,inplace=True)
            backdoor = backdoor.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,backdoor],axis=0)
            i+=backdoor.shape[0]
            backdoor = pd.DataFrame()
    
    if ddos.shape[0] > 0:
        k = random.randint(1,10)
        if(ddos.shape[0] >k):
            all = pd.concat([all,ddos.iloc[:k,:]],axis=0)
            ddos.drop(list(range(k)),axis=0,inplace=True)
            ddos = ddos.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,ddos],axis=0)
            i+=ddos.shape[0]
            ddos = pd.DataFrame()

    if dos.shape[0] > 0:
        k = random.randint(1,10)
        if(dos.shape[0] >k):
            all = pd.concat([all,dos.iloc[:k,:]],axis=0)
            dos.drop(list(range(k)),axis=0,inplace=True)
            dos = dos.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,dos],axis=0)
            i+=dos.shape[0]
            dos = pd.DataFrame()
 
    if mitm.shape[0] > 0:
        k = random.randint(1,10)
        if(mitm.shape[0] >k):
            all = pd.concat([all,mitm.iloc[:k,:]],axis=0)
            mitm.drop(list(range(k)),axis=0,inplace=True)
            mitm = mitm.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,mitm],axis=0)
            i+=mitm.shape[0]
            mitm = pd.DataFrame()

    if password.shape[0] > 0:
        k = random.randint(1,10)
        if(password.shape[0] >k):
            all = pd.concat([all,password.iloc[:k,:]],axis=0)
            password.drop(list(range(k)),axis=0,inplace=True)
            password = password.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,password],axis=0)
            i+=password.shape[0]
            password = pd.DataFrame()

    if ransomware.shape[0] > 0:
        k = random.randint(1,10)
        if(ransomware.shape[0] >k):
            all = pd.concat([all,ransomware.iloc[:k,:]],axis=0)
            ransomware.drop(list(range(k)),axis=0,inplace=True)
            ransomware = ransomware.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,ransomware],axis=0)
            i+=ransomware.shape[0]
            ransomware = pd.DataFrame()

    if scanning.shape[0] > 0:
        k = random.randint(1,10)
        if(scanning.shape[0] >k):
            all = pd.concat([all,scanning.iloc[:k,:]],axis=0)
            scanning.drop(list(range(k)),axis=0,inplace=True)
            scanning = scanning.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,scanning],axis=0)
            i+=scanning.shape[0]
            scanning = pd.DataFrame()

    if xss.shape[0] > 0:
        k = random.randint(1,10)
        if(xss.shape[0] >k):
            all = pd.concat([all,xss.iloc[:k,:]],axis=0)
            xss.drop(list(range(k)),axis=0,inplace=True)
            xss = xss.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,xss],axis=0)
            i+=xss.shape[0]
            xss = pd.DataFrame()

    if injection.shape[0] > 0:
        k = random.randint(1,10)
        if(injection.shape[0] >k):
            all = pd.concat([all,injection.iloc[:k,:]],axis=0)
            injection.drop(list(range(k)),axis=0,inplace=True)
            injection = injection.reset_index(drop=True)
            i+=k
        else:
            all = pd.concat([all,injection],axis=0)
            i+=injection.shape[0]
            injection = pd.DataFrame()

all.to_csv('ton_iot/attacks.csv',index=False)