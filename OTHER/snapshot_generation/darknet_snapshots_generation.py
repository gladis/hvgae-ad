# NOTE: This file generates the snapshots from the darknet dataset. It does everything starting by the cleaning and moving to data spliting.


import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
import pickle


def _to_binary_classification(x):
    if 'Non' in x:
        return 0
    else:
        return 1
        
def _filling_adjacency_numpy(data):
    N = data.shape[0]
    try:
        adjacency = np.zeros((N,N), dtype=bool)
    except Exception as e:
        print(f"An error occurred: {e}")

    source_ips = data['Src IP'].to_numpy()
    destination_ips = data['Dst IP'].to_numpy()
    mask = ((source_ips[:, np.newaxis] == source_ips) | (source_ips[:, np.newaxis] == destination_ips) | (destination_ips[:, np.newaxis] == source_ips)| (destination_ips[:, np.newaxis] == destination_ips) )
    adjacency[mask] = True
    return adjacency

def save_samples(adj,features,labels,adj_path,features_path,labels_path):
        with open(adj_path,'wb') as f:
            pickle.dump(adj,f)
        with open(features_path,'wb') as f:
            pickle.dump(features,f)
        with open(labels_path,'wb') as f:
            pickle.dump(labels,f)

nnodes = 1000
overlap = 0.25

directory = os.path.join(os.getcwd(),f'darknet_snapshots_{int(overlap*100)}_{nnodes}')
os.makedirs(directory)

df = pd.read_csv('other/darknet/all.csv')
df.dropna(axis=0,inplace=True)
df = df.reset_index(drop=True)
df['Label'] = df['Label'].apply(_to_binary_classification)
columns_to_exclude = ['Flow ID', 'Src IP','Src Port', 'Dst IP','Dst Port', 'Timestamp','Label','Label.1','Protocol','Flow Duration']
columns_to_normalize = [x for x in df.columns if x not in columns_to_exclude]
scaler = MinMaxScaler()
df[columns_to_normalize] = scaler.fit_transform(df[columns_to_normalize])

i=0
j=0
while i< df.shape[0]:
    print('snapshot:',j)
    if df.shape[0] > nnodes:
        data = df.iloc[:nnodes,:].copy()
        adj = _filling_adjacency_numpy(data)
        labels = data['Label'].to_numpy()
        data.drop(columns_to_exclude, axis=1, inplace=True)
        features = data.to_numpy()
        save_samples(adj,features,labels,os.path.join(directory,f'adjacency_{j}.pkl'),os.path.join(directory,f'features_{j}.pkl'),os.path.join(directory,f'labels_{j}.pkl'))
        j+=1
        i+=nnodes
        df.drop(range(nnodes - int(nnodes*overlap)),axis=0,inplace=True)
        df = df.reset_index(drop=True)
        print(np.sum(labels),len(labels)-np.sum(labels))
    else:
        adj = _filling_adjacency_numpy(df)
        labels = data['Label'].to_numpy()
        data.drop(columns_to_exclude, axis=1, inplace=True)
        features = data.to_numpy()
        save_samples(adj,features,labels,os.path.join(directory,f'adjacency_{j}.pkl'),os.path.join(directory,f'features_{j}.pkl'),os.path.join(directory,f'labels_{j}.pkl'))
        j+=1
        i+=df.shape[0]
