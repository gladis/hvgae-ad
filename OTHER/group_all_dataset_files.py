# NOTE: add this .py file inside the folder containing all the files of single dataset and run it
# to concatenate all those files into a single one
# this helps sampling a representative data

# this is the first OTHER script used after downloading the datasets


import os 
import pandas as pd




def run():
    directory = os.getcwd()
    files = [f for f in os.listdir(directory) if os.path.isfile(f) and '.py' not in f]
    all = []
    for file in files:
        df = pd.read_csv(file,low_memory=False)
        all.append(df)

    data = pd.concat(all)
    # change the name of the dataset here
    path = os.path.join(directory,'ton_iot.csv')
    data.to_csv(path,index=False)



if __name__ == "__main__":
    run()