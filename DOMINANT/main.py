import os
import pickle
from runner import RUNNER
import numpy as np
import pandas as pd

def load_samples(dataset):
    features_path=os.path.join(os.getcwd(),'datasets',dataset,f'features.pkl')
    adj_path=os.path.join(os.getcwd(),'datasets',dataset,f'adjacency.pkl')
    labels_path=os.path.join(os.getcwd(),'datasets',dataset,f'labels.pkl')

    with open(adj_path,'rb') as f:
        adj = pickle.load(f)
    with open(features_path,'rb') as f:
        features = pickle.load(f)
    with open(labels_path,'rb') as f:
        labels= pickle.load(f)
    return adj,features,labels



if __name__ == "__main__":
    dims = [2,5,10,20]
    outlier_percentage = 0.1
    lr = 0.001
    invert = True
    dataset = 'unsw_nb'
    df = pd.DataFrame(None)
    for dim in dims:
        adj,features,labels = load_samples(dataset=dataset)
        runner = RUNNER(adj=adj,features=features,labels=labels,lr=lr,alpha=0.5,dim=dim)
        loss,accuracy,f1,recall,precision,roc_auc,training_time = runner.fit(outlier_percentage=outlier_percentage,invert=invert)
        print('TRANING:',accuracy,f1,recall,precision,roc_auc,training_time)
        loss,accuracy,f1,recall,precision,roc_auc = runner.predict(outlier_percentage=outlier_percentage,invert=invert)
        print('TESTING:',accuracy,f1,recall,precision,roc_auc)
        df[f'acc_{dim}'] = [round(accuracy,4)]
        df[f'f1_{dim}'] = [round(f1,4)]
        df[f'rec_{dim}'] = [round(recall,4)]
        df[f'pre_{dim}'] = [round(precision,4)]
        df[f'roc_{dim}'] = [round(roc_auc,4)]
        df[f'tt_{dim}'] = [round(training_time,4)]
    print(df)
    df.to_csv(f'{dataset}_results.csv')

